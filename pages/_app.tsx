import '@assets/main.scss'
import 'keen-slider/keen-slider.min.css'
import '@assets/chrome-bug.css'

import { FC, useEffect } from 'react'
import type { AppProps } from 'next/app'

import { ManagedUIContext } from '@components/ui/context'
import { Head } from '@components/common'
import StateProvider, { StateReducer } from 'providers/StateProvider'
import { initialState } from 'providers/StateProvider/StateReducer'
import { FirebaseAppProvider } from 'reactfire'

const Noop: FC = ({ children }) => <>{children}</>

const firebaseConfig = {
  apiKey: "AIzaSyC5517qc3-MV69dl0BgVGVP9sMQk89ZAUQ",
  authDomain: "malgudis-281f7.firebaseapp.com",
  projectId: "malgudis-281f7",
  storageBucket: "malgudis-281f7.appspot.com",
  messagingSenderId: "29594597012",
  appId: "1:29594597012:web:33df53adec8c3e29c42681",
  measurementId: "G-XVTB1F02X5"
};

export default function MyApp({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout || Noop

  useEffect(() => {
    document.body.classList?.remove('loading')
  }, [])

  return (
    <>
      <Head />
      <StateProvider reducer={StateReducer} initialState={initialState}>
        <ManagedUIContext>
          <Layout pageProps={pageProps}>
            <FirebaseAppProvider firebaseConfig={firebaseConfig}>
              <Component {...pageProps} />
            </FirebaseAppProvider>
          </Layout>
        </ManagedUIContext>
      </StateProvider>
    </>
  )
}
